---
author: 'Dr. John Noll'
course: 7COM1079
date: '\today'
institute: University of Hertfordshire
module: 7COM1079
subtitle: '7COM1079 -- Team Research and Development Project'
term: 'fall-19'
title: Correlation
---

Instructions for completing this practical exercise:

1.  Compile the lab instructions

    1.  If you are reading this *README.md* file from the web, you are
        not playing the game!

    Open a Command shell and change to the Git workspace containing this
    file, that you just cloned:

         #. Click the "windows" button, type "command," then open a "DOS
          box" (the black window).

         #. At the command prompt, type:

                 cd correlation-lab

             *Note:* If you did not clone the correlation-lab repository in
              your "home" directory, you should type:

                 cd \Users\yourusername\path\to\correlation-lab

             **IMPORTANT!**  replace "\\path\\to" above with the path to the
         directory into which you cloned the correlation-lab repository.

    1.  In the command window, type:

             pandoc --standalone -t html -o instructions.html instructions.md

    2.  Now, just type:

             instructions.html

        to view the instructions in a web browser.

2.  Read and follow the instructions.
