---
author: 'Dr. John Noll'
course: 7COM1079
date: '\today'
institute: University of Hertfordshire
module: 7COM1079
subtitle: '7COM1079 -- Team Research and Development Project'
term: 'fall-19'
title: Correlation
---

Overview
========

We're going to investigate correlation between home ownership and other
variables in London. Once again, we'll use the [London Borough Profiles
dataset](http://jnoll.nfshost.com/7COM1079-fall-19/topics/Visualization/london-borough-profiles-2016_Data_set.csv).

Load and set-up the dataset
---------------------------

Recall from last week that you have to import the dataset as a CSV file,
then ensure the variables of interest are recognized correctly.

This week we are interested in (at least) *HomesOwnedOutright2014* and
*AverageAge2016*.

Display a scatterplot
---------------------

1.  Recall that we always use the "Legacy Dialogs" options:

    ![Legacy dialogs](spss-legacy-dialogs.png)\

2.  Select "Scatter/Dot":

    ![Scatter/Dot](spss-legacy-dialogs-scatter.png)\

3.  A "Simple Scatter" is sufficient:

    ![Simple scatter](spss-legacy-dialogs-scatter-dialog.png)\

4.  Click "Define" to specify the axes:

    ![Define](spss-legacy-dialogs-scatter-dialog-simple.png)\

5.  The "Define" dialog lets you specify the X and Y axes, among other
    things:

    ![Define dialog](spss-legacy-dialogs-scatter-define-dialog.png)\

6.  Select HomesOwnedOutright2014 vs AverageAge2016:

    ![Homes vs. Age](spss-legacy-dialogs-scatter-define-axes.png)\

7.  Are they correlated?

    ![Scatterplot output](spss-scatterplot.png)\

8.  Plot a "reference line" (right click the scatterplot):

    ![Right click](spss-scatterplot-ref-define.png)\

9.  Select "Reference Line from Equation" from the "Options" menu:

    ![Ref line](spss-scatterplot-ref-define-2.png)\

10. You'll be given the option to refine the equation, but the default
    is OK

    ![Refine ref line equation](spss-scatterplot-ref-define-3.png)\

Is Home Ownership Correlated With Age?
--------------------------------------

1.  Select "Analyze -\> Correlate -\> Bivariate" to compare one
    dependent to one independent variable.

    ![Analyze-\>Correlate-\>Bivariate](spss-analyze-correlate.png)\

2.  We want to compare *AverageAge2016* with *HomesOwnedOutright2014*

    ![Correlation](spss-analyze-correlate-vars.png)\

3.  Are they *significantly* correlated?

    ![Correlation output](spss-analyze-correlate-output.png)\

    Non-parametric tests:

    ![Non-parametric correlation
    output](spss-analyze-correlate-non-parametric-output.png)\

4.  How significant?

    ![How significant?](spss-analyze-correlate-p-value-expand.png)\

Test some other relationships
-----------------------------

1.  Is there a (negative) correlation between percentage of population
    born abroad and home ownership?

2.  Is there a (negative) correlation between home ownership and crime?

3.  Is there a (positive) correlation between home ownership and
    recycling rate?

4.  Is there a (positive) correlation between home ownership and GCSE
    results?

5.  Is there a (positive) correlation between home ownership and
    measures of happiness, satisfaction, "worthwhileness," etc.?
